package no.ntnu.imt3281.eksamen;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.ComboBox;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import no.ntnu.imt3281.eksamen.exchange_rates.ExchangeRates;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.*;

/**
 * Controller for Convert2.fxml
 * Controls LineChart and ComboBox
 */
public class Convert2Controller {

    @FXML
    private VBox vBox;

    @FXML
    private ComboBox<String> comboBox;

    @FXML
    private LineChart<String, Number> chart;
    // Used to remember previous graph for removal
    private XYChart.Series<String, Number> series;

    private ExchangeRates rates;
    private ObservableList<String> currencies;

    private ResourceBundle properties;

    /**
     * Constructor, creates an ExchangeRates-object from URL and fills ObservableList 'currencies'
     */
    public Convert2Controller(){

        properties = ResourceBundle.getBundle("no.ntnu.imt3281.eksamen.I18N.i18n");

        // Create ExchangeRates-object
        try {
            /*rates = new ExchangeRates(new URL("https://data.norges-bank.no/api/data/EXR/B..NOK.SP?startPeriod=2019-09-09&endPeriod=2019-10-09&format=sdmx-json&locale=no"));*/
            LocalDate today = LocalDate.now();
            LocalDate previousWeek = today.minus(1, ChronoUnit.WEEKS);
            rates = new ExchangeRates(new URL("https://data.norges-bank.no/api/data/EXR/B..NOK.SP?startPeriod=" + previousWeek + "&endPeriod=" + today + "&format=sdmx-json&locale=no"));

            // Get acronyms of currencies
            String[] currencyAcronyms = rates.getCurrencies();
            List<String> currencyList = new ArrayList<>();

            // Loop through all acronyms, get description and add it to 'currencyList'
            for (String currency : currencyAcronyms) {
                String name = rates.getCurrencyDescription(currency);
                currencyList.add(currency + " (" + name + ")");
            }
            // Create ObservableArrayList from currencyList
            currencies = FXCollections.observableArrayList(currencyList);

        } catch (MalformedURLException e){
            e.printStackTrace();
        }
    }

    /**
     * initializes connection between comboBox and observableList, and setOnAction
     * Also loads and adds Convert.fxml
     */
    public void initialize(){

        comboBox.setItems(currencies);
        comboBox.setOnAction(e -> fillChart());

        // Do not show dots on graph
        chart.setCreateSymbols(false);

        // Load Convert.fxml and add to VBox
        FXMLLoader loader = new FXMLLoader(getClass().getResource("Convert.fxml"));
        try {
            AnchorPane pane = loader.load();
            vBox.getChildren().add(pane);
        } catch(IOException e){
            e.printStackTrace();
        }
    }

    /**
     * Fills chart with dates and exchangerates from previous 30 days
     */
    @FXML
    private void fillChart(){

        // Remove old data
        chart.getData().remove(series);

        // Find chosen currency, and acronym
        String choice = comboBox.getSelectionModel().getSelectedItem();
        String currency = choice.split(" ")[0];

        try {
            Date[] dates = rates.getDates();
            Double[] theRates = rates.getExchangeRates(currency);
            series = new XYChart.Series<>();
            series.setName(properties.getString("chartText") + " " + choice);

            // Loop through dates and rates, add them to 'series'
            for(int i = 0; i < dates.length; i++){

                // Format date
                String formattedDate = new SimpleDateFormat("dd-MM-yyyy").format(dates[i]);

                // Add date and rate
                series.getData().add(new XYChart.Data<>(formattedDate, theRates[i]));

                // Skip some dates
                /*i += 1;*/
            }

            // Add to chart
            chart.getData().add(series);

        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}
