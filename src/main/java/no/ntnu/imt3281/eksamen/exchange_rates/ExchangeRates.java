package no.ntnu.imt3281.eksamen.exchange_rates;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

/**
 * Reads JSON from url to norges bank or JSON-string
 * Holds information
 */
public class ExchangeRates {

    // Holds relevant information from url/JSON-string
    private JSONArray currencyValues;
    private JSONArray dateValues;
    private JSONObject exchangeRateSeries;

    /**
     * Constructor with JSON-string parameter
     * Calls function that extracts relevant info
     * @param JSON String containing information
     */
    public ExchangeRates(String JSON){

        traverseAndExtractInfo(JSON);
    }

    /**
     * Constructor with URL-parameter
     * Reads JSON String from url
     * Calls function that extracts relevant info
     * @param url URL to information
     */
    public ExchangeRates(URL url){

        try {
            // Next line found in answer of ccleve on https://stackoverflow.com/questions/4328711/read-url-to-string-in-few-lines-of-java-code
            String JSON = new Scanner(url.openStream(), StandardCharsets.UTF_8).useDelimiter("\\A").next();
            traverseAndExtractInfo(JSON);
        } catch(IOException e){
            e.printStackTrace();
        }
    }

    /**
     * Navigates through JSON hierarchy and extracts relevant JSONArrays and a JSONObject
     * @param JSON String containing information
     */
    private void traverseAndExtractInfo(String JSON){

        JSONObject info = new JSONObject(JSON);

        // Navigate down hierarchy
        JSONObject structure = info.getJSONObject("structure");
        JSONObject dimensions = structure.getJSONObject("dimensions");
        JSONObject series = (JSONObject) dimensions.getJSONArray("series").get(1);

        // Extract relevant info
        currencyValues = series.getJSONArray("values");
        dateValues = dimensions.getJSONArray("observation").getJSONObject(0).getJSONArray("values");
        exchangeRateSeries = info.getJSONArray("dataSets").getJSONObject(0).getJSONObject("series");
    }

    /**
     * Checks whether a currency exists or not
     * @param currency String, acronyms 'USD', 'AUD', etc..
     * @return True if currency exists, false otherwise
     */
    boolean currencyExists(String currency){

        // Loop through values-array, return true if currency found
        for (Object o : currencyValues) {
            JSONObject json = (JSONObject) o;

            if(json.getString("id").equals(currency)) {
                return true;
            }
        }
        // Currency was not found
        return false;
    }

    /**
     * Gets description of currency acronyms
     * @return description of acronyms 'USD', 'AUD', etc..(or null if currency not found)
     */
    public String getCurrencyDescription(String currency) {

        // Loop through values-array, return description
        for (Object o : currencyValues) {
            JSONObject json = (JSONObject) o;

            if(json.getString("id").equals(currency)) {
                return json.getString("name");
            }
        }
        // Currency was not found
        return null;
    }

    /**
     * Gets dates for observations
     * @return Array of Date-objects for observations
     * @throws ParseException if there is something wrong with a date
     */
    public Date[] getDates() throws ParseException {

        Date[] dates = new Date[dateValues.length()];
        Date date;
        int counter = 0;

        // Loop through values-array
        JSONObject json;
        for (Object o : dateValues) {
            json = (JSONObject) o;

            // Extract date, create Date-object and insert in array
            String temp = json.getString("name");
            date = new SimpleDateFormat("yyyy-MM-dd").parse(temp);

            dates[counter] = date;
            counter++;
        }
        return dates;
    }

    /**
     * Gets exhcangerates for given currency
     * @param currency String, acronyms 'USD', 'AUD', etc..
     * @return Array of doubles
     */
    public Double[] getExchangeRates(String currency){

        int index;
        // If currency was found(index != -1)
        if((index = getIndex(currency)) != -1){

            JSONObject jsonRates = exchangeRateSeries.getJSONObject("0:" + index + ":0:0").getJSONObject("observations");
            Double[] rates = new Double[jsonRates.length()];

            // Add all rates to array
            for (int i = 0; i < jsonRates.length(); i++) {
                rates[i] = Double.parseDouble(jsonRates.getJSONArray(Integer.toString(i)).get(0).toString());
            }
            return rates;
        }
        // Currency was not found(index == -1)
        return null;
    }

    /**
     * Gets index of currency
     * @param currency acronyms 'USD', 'AUD', etc..
     * @return int, index (or -1 if currency not found)
     */
    private int getIndex(String currency){

        JSONObject json;

        // Loop through all currencies
        for(int i = 0; i < currencyValues.length(); i++){
            json = currencyValues.getJSONObject(i);

            // If currency found
            if(json.getString("id").equals(currency)){
                return i;
            }
        }
        // Currency not found
        return -1;
    }

    /**
     * Exchanges an amount of NOK to other currency
     * @param amount long, amount of NOK
     * @param toCurrency String, acronyms 'USD', 'AUD', etc..
     * @return amount in other currency
     */
    public Double exchangeTo(long amount, String toCurrency){

        // Get rates
        Double[] rates = getExchangeRates(toCurrency);

        // Get latest rate
        Double rate = rates[rates.length - 1];

        // Calculate
        return ((rate <= 10)? amount/rate : (amount/rate)*amount);
    }

    /**
     * Exchanges an amount of other currency to NOK
     * @param amount long, amount of other currency
     * @param fromCurrency String, acronyms 'USD', 'AUD', etc..
     * @return amount in NOK
     */
    Double exchangeFrom(long amount, String fromCurrency){

        // Get rates
        Double[] rates = getExchangeRates(fromCurrency);

        // Get latest rate
        Double rate = rates[rates.length - 1];

        // Calculate
        return (rate <= 10)? amount*rate : (amount*rate)/amount;
    }

    /**
     * Gets all existing currencies
     * @return Array of Strings containing currencies(acronyms 'USD', 'AUD', etc..)
     */
    public String[] getCurrencies(){

        String[] currencies = new String[currencyValues.length()];
        int counter = 0;

        // Loop through all currencies and add their id's to array
        for (Object o : currencyValues) {
            JSONObject json = (JSONObject) o;
            currencies[counter] = json.get("id").toString();
            counter++;
        }

        return currencies;
    }
}
