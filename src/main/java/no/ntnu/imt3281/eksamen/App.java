package no.ntnu.imt3281.eksamen;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Application for converting NOK to other currency and showing chart of exchange rate over time
 */
public class App extends Application {


    @Override
    public void start(Stage stage) throws IOException {

        FXMLLoader loader = new FXMLLoader(getClass().getResource("Convert2.fxml"));

        AnchorPane root = loader.load();

        // Set scene and launch stage
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.setTitle("Currency-calculator");
        stage.show();
    }

    /**
     * Main, launches application
     * @param args command-line arguments(not used)
     */
    public static void main(String[] args) {
        launch();
    }
}
