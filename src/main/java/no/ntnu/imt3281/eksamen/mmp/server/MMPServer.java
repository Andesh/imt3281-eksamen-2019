package no.ntnu.imt3281.eksamen.mmp.server;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.*;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * A fully functioning server for a MMP network. Clients can connect to the server and subscribe to channels/topics
 * and receive messages sent to that channel. Clients can also publish messages to channels (both subscribed and
 * unsubscribed.)
 */
public class MMPServer {

    private boolean requireUnamePwd;

    private final List<Client> clients;

    private ArrayBlockingQueue<Client> disconnectedClients = new ArrayBlockingQueue<>(100);

    private boolean stopping = false;

    private static final Logger LOGGER = MyLogger.getLogger();

    // Used for communication
    private static final String TOPIC = "topic";
    private static final String CONNECTED = "connected";

    /**
     * Start the MMPServer, if the passed parameter is true the server is to require username/password
     * to accept connections otherwise username/password is not require.
     *
     * Starts threads for ServerSocket and for listening
     *
     * @param requireUnamePwd true if the server should require username/password, false means no username/password is required.
     */
    public MMPServer(boolean requireUnamePwd) {

        this.requireUnamePwd = requireUnamePwd;

        clients = new ArrayList<>();

        // Create and start server-thread
        Thread serverThread = new Thread(() -> {

           try(ServerSocket server = new ServerSocket(4567)){

               while(!stopping){
                   Socket s = server.accept();

                   // Create BufferedReader and Writer
                   BufferedReader br = new BufferedReader(new InputStreamReader(s.getInputStream()));
                   BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(s.getOutputStream()));

                   // Create Client-object and add to list
                   Client client = new Client(bw, br);
                   addClient(client);
               }

           } catch (IOException e) {
               LOGGER.log(Level.INFO, e.getMessage(), e);
           }
        });
        serverThread.setDaemon(true);
        serverThread.start();

        // Thread listening for messages from clients
        Thread listen = new Thread(() -> {

            while(!stopping){
                synchronized(clients){

                    try {
                        // Check if there are any clients to remove, if so, remove client and send reply
                        if (!disconnectedClients.isEmpty()) {

                            Client client = disconnectedClients.take();
                            clients.remove(client);

                            // Create and send reply
                            JSONObject disconnected = new JSONObject();
                            disconnected.put("disconnected", "OK");
                            client.write(disconnected.toString());
                        }

                        // Loop through all clients
                        for (Client client : clients) {

                            String msg = client.read();
                            if (msg != null) {
                                handleMessage(msg, client);
                            }
                        }
                    } catch(IOException | InterruptedException e){
                        LOGGER.log(Level.INFO, e.getMessage(), e);
                    }
                }
            }
        });
        listen.setDaemon(true);
        listen.start();
    }

    /**
     * Handles messages from clients
     * @param msg to-stringed JSONObject
     * @param client Client
     */
    private void handleMessage(String msg, Client client){

        // Convert to JSON and extract 'action'
        JSONObject json = stringToJSON(msg);
        String action = json.get("action").toString();

        switch (action){
            case "disconnect":{
                // Add client to queue, client is removed by the Thread 'listen'
                if(!disconnectedClients.contains(client)){
                    disconnectedClients.add(client);
                }
                break;
            }
            case "subscribe":{
                String topic = json.get(TOPIC).toString();
                client.subscribe(topic);
                json = new JSONObject();
                json.put("subscribed", topic);
                break;
            }
            case "unsubscribe":{
                String topic = json.get(TOPIC).toString();
                client.unsubscribe(topic);
                json = new JSONObject();
                json.put("unsubscribed", topic);

                break;
            }
            case "publish":{
                messageSubscribers(json, client);
                break;
            }
            default: {
                break;
            }
        }
        // If action is subscribe or unsubscribe, reply
        if(action.matches("subscribe|unsubscribe")){
            try{
                client.write(json.toString());
            } catch(IOException e){
                LOGGER.log(Level.INFO, e.getMessage(), e);
            }
        }
    }

    /**
     * Reads the connect message from the client and checks the protocol and version number. If the protocol and
     * version information is correct the username/password is checked (if the server is started in that mode).
     * If all information is correct the client is added to the list of clients, and a connected: OK message
     * is sent to the client.
     * If any information is incorrect, a message informing the client about the error is sent back and the
     * client is NOT added to the list of client.
     *
     * @param client a Client object which holds the reader/writer for communicating with the client.
     */
    void addClient(Client client) {

        boolean success = false;
        JSONObject answer;

        try {
            String str = client.read();
            // Convert String to JSONObject, extract protocol and version
            JSONObject json = stringToJSON(str);
            String protocol = json.get("protocol").toString();
            String version = json.get("version").toString();

            // If protocol and version is correct
            if("MMP".equals(protocol) && "0.1".equals(version)){

                // If server requires username/password
                if(requireUnamePwd){

                    // Extract user
                    JSONObject user = (JSONObject) json.get("user");
                    try{
                        // Extract username and password
                        String userName = user.get("uname").toString();
                        String password = user.get("pwd").toString();

                        // If correct username/password combination
                        if("very".equals(userName) && "secret".equals(password)){
                            success = true;
                        }

                    } catch(NullPointerException ignored){}
                } else{
                    // Server does not require username/password
                    success = true;
                }

                if(success){
                    clients.add(client);
                    answer = new JSONObject();
                    answer.put(CONNECTED, "OK");
                } else{
                    answer = new JSONObject();
                    answer.put(CONNECTED, "FAIL");
                    answer.put("reason", "Bad username/password");
                }
            } else{
                // Protocol/version is not correct
                answer = new JSONObject();
                answer.put(CONNECTED, "FAIL");
                answer.put("reason", "Bad protocol/protocol version");
            }

            // Send reply to client
            client.write(answer.toString());

        } catch(IOException e){
            LOGGER.log(Level.INFO, e.getMessage(), e);
        }
    }

    /**
     * Messages subscribers of a channel
     * Also sends confirmation to the client who sent message
     * @param json JSONObject containing 'topic' and 'subject'
     * @param client Client, the client who sent message
     */
    private void messageSubscribers(JSONObject json, Client client){

        // Extract topic and subject
        String topic = json.get(TOPIC).toString();
        String subject = json.get("subject").toString();

        // Create list of subscribers
        List<Client> subscribers = new ArrayList<>();

        // Loop through clients and add subscribers to list
        for (Client c: clients) {
            if(c.isSubscribed(topic)){
                subscribers.add(c);
            }
        }

        // Create message for subscribers
        JSONObject message = new JSONObject();
        JSONObject msg = new JSONObject();
        msg.put(TOPIC, topic);
        msg.put("subject", subject);
        message.put("message", msg);

        // Create message for the client who sent message
        JSONObject message2 = new JSONObject();
        message2.put("published", topic);
        message2.put("recipients", subscribers.size());
        try {
            // Loop through subscribers and send message
            for (Client c : subscribers) {
                c.write(message.toString());
            }
            // Send message to the client who sent message
            client.write(message2.toString());

        } catch(IOException e){
            LOGGER.log(Level.INFO, e.getMessage(), e);
        }
    }

    /**
     * Converts string to JSONObject(Taken from Server.java in https://bitbucket.org/Andesh/imt3281-project2-2019/src/master/)
     * @param str String to be converted
     * @return JSONObject with str contents
     */
    private JSONObject stringToJSON(String str) {

        JSONObject message = null;
        try {
            JSONParser parser = new JSONParser();
            message = (JSONObject) parser.parse(str);
        } catch (ParseException e) {
            LOGGER.log(Level.INFO, e.getMessage(), e);
        }
        return message;
    }

    /**
     * A class used to represent a single client on the server. Contains the reader and writer used to communicate
     * with the client as well as a list of channels/topics that this client is subscribed to.
     */
    public static class Client {
        BufferedWriter bw = null;
        BufferedReader br = null;
        LinkedList<String> channels = new LinkedList<>();

        /**
         * Creates a new Client object, takes the reader and writer used to communicate with the client
         * as parameters and stores them in the object.
         *
         * @param bw the writer used to send messages to the client
         * @param br the reader used to receive message from the client
         */
        public Client (BufferedWriter bw, BufferedReader br) {
            this.br = br;
            this.bw = bw;
        }

        /**
         * Convenience method used to read messages from the client. Checks the reader if data is available and
         * if so return that data, if no data is available return null.
         *
         * @return any available data or null if no data is available.
         * @throws IOException if there are problems reading from the client.
         */
        public String read () throws IOException {
            if (br.ready()) {
                return br.readLine();
            }
            return null;
        }

        /**
         * Convenience method used to send data to the client, makes sure all data is followed by a newline character and
         * that the buffer is flushed when data has been sent.
         *
         * @param msg the message to send to the client.
         * @throws IOException if there are problems sending data to the client.
         */
        public void write(String msg) throws IOException {
            bw.write(msg);
            bw.newLine();
            bw.flush();
        }

        /**
         * Subscribes client to channel
         * @param channel String
         */
        public void subscribe(String channel){
            channels.add(channel);
        }

        /**
         * Unsubscribes client from channel
         * @param channel String
         */
        public void unsubscribe(String channel){
            channels.remove(channel);
        }

        /**
         * Checks if a client is subscribed to a channel
         * @param channel String, topic of channel
         * @return True if subscribed, false otherwise
         */
        public boolean isSubscribed(String channel){
            return(channels.contains(channel));
        }
    }
}
