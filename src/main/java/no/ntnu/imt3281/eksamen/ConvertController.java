package no.ntnu.imt3281.eksamen;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import no.ntnu.imt3281.eksamen.exchange_rates.ExchangeRates;

import java.net.MalformedURLException;
import java.net.URL;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Controller for Convert.fxml
 */
public class ConvertController {

    @FXML
    private TextField input;

    @FXML
    private TextField output;

    @FXML
    private ComboBox<String> dropdown;

    private ExchangeRates rates;
    private ObservableList<String> currencies;

    /**
     * Constructor, creates an ExchangeRates-object and fills ObservableList 'currencies'
     */
    public ConvertController(){

        // Create ExchangeRates-object
        try {
            /*rates = new ExchangeRates(new URL("https://data.norges-bank.no/api/data/EXR/B..NOK.SP?startPeriod=2019-09-27&endPeriod=2019-10-04&format=sdmx-json&locale=no"));*/

            LocalDate today = LocalDate.now();
            LocalDate previousWeek = today.minus(1, ChronoUnit.WEEKS);
            rates = new ExchangeRates(new URL("https://data.norges-bank.no/api/data/EXR/B..NOK.SP?startPeriod=" + previousWeek + "&endPeriod=" + today + "&format=sdmx-json&locale=no"));

            // Get acronyms of currencies
            String[] currencyAcronyms = rates.getCurrencies();
            List<String> currencyList = new ArrayList<>();

            // Loop through all acronyms, get description and add it to 'currencyList'
            for (String currency : currencyAcronyms) {
                String name = rates.getCurrencyDescription(currency);
                currencyList.add(currency + " (" + name + ")");
            }
            // Create ObservableArrayList from currencyList
            currencies = FXCollections.observableArrayList(currencyList);

        } catch (MalformedURLException e){
            e.printStackTrace();
        }
    }

    /**
     * Initializes application
     */
    public void initialize(){

        dropdown.setItems(currencies);
        dropdown.setOnAction(e -> calculate());
        input.setOnKeyReleased(e -> calculate());
    }

    /**
     * Converts NOK to another currency
     * Reads input(NOK) and selected currency, then converts
     */
    @FXML
    private void calculate(){

        // Read input and selected currency if not empty
        String in = input.getText();
        String choice = dropdown.getSelectionModel().getSelectedItem();

        // If input is not empty, input is a number and a currency is chosen
        if(!"".equals(in) && in.matches("\\d+") && choice != null) {

            // Find selected currency
            String toCurrency = choice.split(" ")[0];

            // Convert input to long and calculate
            long amount = Long.parseLong(in);
            Double answer = rates.exchangeTo(amount, toCurrency);

            // Output answer
            output.setText(String.format("%1$,.2f", answer));
            // String.format found in answer of David Tang: https://stackoverflow.com/questions/4885254/string-format-to-format-double-in-java
        } else{
            output.setText("");
        }
    }
}